﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Assignment_Work
{
    public partial class Form1 : Form
    {

        #region Variables

        Bitmap objOutputBitMap = new Bitmap(530, 440);
        Canvas objCanvas;

        #endregion

        #region Form Intialization

        /// <summary>
        /// Form object
        /// Load Initial Setting for the Form
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            objCanvas = new Canvas(Graphics.FromImage(objOutputBitMap));
        }

        #endregion

        #region Form Events

        /// <summary>
        /// Picture Box Paint Event
        /// used for draw shape on Picture Box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OutputWindow_Paint(object sender, PaintEventArgs e)
        {
            Graphics graphics = e.Graphics;
            graphics.DrawImageUnscaled(objOutputBitMap, 0, 0);

        }

        /// <summary>
        /// Run Multiline Command Click Event
        /// Draw Shapes based on multiple command inputs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnRunCommand_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtProgramWindow.Text))
                {
                    HideErrorMsg();
                    RunMultiLineCommand(txtProgramWindow.Lines);
                }
            }
            catch (Exception ex)
            {
                ShowErrorMsg(ex.Message);
            }
        }

        /// <summary>
        /// Check For Command is valid or not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChkSyntax_Click(object sender, EventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(txtProgramWindow.Text))
                {
                    HideErrorMsg();
                    ValidateCommand objIsValid = new ValidateCommand(txtProgramWindow.Lines);
                    if (!objIsValid.IsCommandValid)
                    {
                        ShowErrorMsg(objIsValid.ErrorMessage);
                    }
                    else
                    {
                        ShowErrorMsg("All the Command are correct.");
                    }
                }

            }
            catch (Exception ex)
            {
                ShowErrorMsg(ex.Message);
            }
        }

        /// <summary>
        /// Single Line Command Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commandLine_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Enter && !string.IsNullOrEmpty(txtcommandLine.Text))
                {
                    HideErrorMsg();
                    RunCommand(txtcommandLine.Text.Trim().ToLower());
                }
            }
            catch (Exception ex)
            {
                ShowErrorMsg(ex.Message);
            }

        }

        #endregion

        #region Methods and Functions

        /// <summary>
        /// Draw Shpes based on given command
        /// RunCommand Function
        /// </summary>
        /// <param name="commandText"></param>
        public void RunCommand(string CommandText)
        {
            ValidateCommand objIsValid = new ValidateCommand(CommandText);

            if (objIsValid.IsCommandValid)
            {
                objCanvas.Command = CommandText.Split(' ')[0];

                if (CommandText.Split(' ').Length > 1)
                    objCanvas.Parameters = CommandText.Split(' ')[1].Split(',');

                if (objCanvas.Command.Equals("reset"))
                {
                    objCanvas.Command = "clear";
                    Commands obj = new Commands(objCanvas);
                    txtProgramWindow.Text = "";
                }
                else if (objCanvas.Command.Equals("pen"))
                {
                    SetPenColor(objCanvas.Parameters[0]);
                }
                else if (objCanvas.Command.Equals("fill"))
                {
                    FillShapes(objCanvas.Parameters[0]);
                }
                else
                {
                    Commands objCommand = new Commands(objCanvas);
                }

                txtcommandLine.Text = "";
                Refresh();
            }
            else
            {
                ShowErrorMsg(objIsValid.ErrorMessage);
            }
        }

        /// <summary>
        /// Set Pen Color
        /// </summary>
        /// <param name="color"></param>
        public void SetPenColor(string color)
        {
            if (color.Equals("green"))
            {
                objCanvas.pen = new Pen(Color.Green, 1);
            }
            else if (color.Equals("red"))
            {
                objCanvas.pen = new Pen(Color.Red, 1);
            }
            else if (color.Equals("blue"))
            {
                objCanvas.pen = new Pen(Color.Blue, 1);
            }
            else if (color.Equals("black"))
            {
                objCanvas.pen = new Pen(Color.Black, 1);
            }
        }

        /// <summary>
        /// Fill Setting - On/Off
        /// Filled Shape with Color
        /// </summary>
        /// <param name="fill"></param>
        public void FillShapes(string fill)
        {
            if (fill.Equals("on"))
            {
                objCanvas.Fill = "on";
            }
            else if (fill.Equals("off"))
            {
                objCanvas.Fill = "off";
            }
        }


        /// <summary>
        /// Show Error Message on Screen
        /// Message - Validation Message
        /// </summary>
        /// <param name="message"></param>
        public void ShowErrorMsg(string message)
        {
            this.lblErrorMsg.Visible = true;
            this.lblErrorMsg.Text = message;
        }

        /// <summary>
        /// Hide Error Message
        /// </summary>
        public void HideErrorMsg()
        {
            this.lblErrorMsg.Visible = false;
        }

        /// <summary>
        /// Multiline Command Execution
        /// </summary>
        /// <param name="multiCommandText"></param>
        public void RunMultiLineCommand(string[] multiCommandText)
        {
            ValidateCommand objIsValid = new ValidateCommand(multiCommandText);

            if (objIsValid.IsCommandValid)
            {
                foreach (string commandText in multiCommandText)
                {
                    RunCommand(commandText.ToString().ToLower());
                }
            }
            else
            {
                ShowErrorMsg(objIsValid.ErrorMessage);
            }
        }

        #endregion

    }
}
