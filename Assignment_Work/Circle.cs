﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assignment_Work
{
    /// <summary>
    /// Responsible for To Draw Circle
    /// Have a Single Responsibility to Draw Circel Based on Paramter
    /// Used IRunCommand Interface 
    /// </summary>
    public class Circle : IRunCommand
    {
        /// <summary>
        /// Constructor with Parameter as Canvas
        /// </summary>
        /// <param name="canvas"></param>
        public Circle(Canvas canvas)
        {
            this.RunCommand(canvas);
        }

        /// <summary>
        /// Draw Circle
        /// </summary>
        /// <param name="canvas"></param>
        public void RunCommand(Canvas canvas)
        {
            int radius = Convert.ToInt32(canvas.Parameters[0]);

            canvas.graphics.DrawEllipse(canvas.pen, new Rectangle(canvas.xPosition - (radius / 2), canvas.yPosition - (radius / 2), radius, radius));
            if (canvas.Fill.Equals("on"))
            {
                System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(canvas.pen.Color);
                canvas.graphics.FillEllipse(myBrush, new Rectangle(canvas.xPosition - (radius / 2), canvas.yPosition - (radius / 2), radius, radius));
            }
        }
    }
}
