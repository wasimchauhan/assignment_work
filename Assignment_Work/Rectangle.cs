﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Work
{
    /// <summary>
    /// Responsible for Rectangle Command
    /// </summary>
    public class Rect : IRunCommand
    {
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="canvas"></param>
        public Rect(Canvas canvas)
        {
            this.RunCommand(canvas);
        }

        /// <summary>
        /// Draw Rectangle on Canvas
        /// </summary>
        /// <param name="canvas"></param>
        public void RunCommand(Canvas canvas)
        {
            int width = Convert.ToInt32(canvas.Parameters[0]);
            int height = Convert.ToInt32(canvas.Parameters[1]);

            canvas.graphics.DrawRectangle(canvas.pen, canvas.xPosition - (width / 2), canvas.yPosition - (height / 2), width, height);
            if (canvas.Fill.Equals("on"))
            {
                System.Drawing.SolidBrush myBrush = new System.Drawing.SolidBrush(canvas.pen.Color);
                canvas.graphics.FillRectangle(myBrush, canvas.xPosition - (width / 2), canvas.yPosition - (height / 2), width, height);
            }

        }
    }
}
