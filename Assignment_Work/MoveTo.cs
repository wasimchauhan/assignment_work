﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assignment_Work
{
    /// <summary>
    /// Responsible for MoveTo Command Execution
    /// </summary>
    public class MoveTo : IRunCommand
    {
        /// <summary>
        /// Constructor of Class
        /// </summary>
        /// <param name="canvas"></param>
        public MoveTo(Canvas canvas)
        {
            this.RunCommand(canvas);
        }

        /// <summary>
        /// Set Pointer in Graphics Area
        /// </summary>
        /// <param name="canvas"></param>
        public void RunCommand(Canvas canvas)
        {
            canvas.graphics.DrawRectangle(new Pen(Color.Red, 1), Convert.ToInt32(canvas.Parameters[0]), Convert.ToInt32(canvas.Parameters[1]), 2, 2);
            canvas.xPosition = Convert.ToInt32(canvas.Parameters[0]);
            canvas.yPosition = Convert.ToInt32(canvas.Parameters[1]);
        }
    }
}
