﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Work
{
    /// <summary>
    /// Responsible for Drawing Shapes on Graphics
    /// </summary>
    public class Commands
    {
        /// <summary>
        /// Constructor of the class
        /// </summary>
        /// <param name="canvas"></param>
        public Commands(Canvas canvas)
        {
            RunCommand(canvas);
        }

        /// <summary>
        /// Run the Command 
        /// Draw the Shape based on given Command
        /// </summary>
        /// <param name="objCanvas"></param>
        public void RunCommand(Canvas objCanvas)
        {
            if (objCanvas.Command.Equals("clear"))
            {
                Clear objClear = new Clear(objCanvas);
            }
            else if (objCanvas.Command.Equals("moveto"))
            {
                MoveTo objMoveTo = new MoveTo(objCanvas);
            }
            else if (objCanvas.Command.Equals("drawto"))
            {
                DrawTo ObjDrawTo = new DrawTo(objCanvas);
            }
            else if (objCanvas.Command.Equals("rect"))
            {
                Rect ObjRect = new Rect(objCanvas);
            }
            else if (objCanvas.Command.Equals("circle"))
            {
                Circle ObjCircle = new Circle(objCanvas);
            }
            else if (objCanvas.Command.Equals("tri"))
            {
                Triangle ObjTriangle = new Triangle(objCanvas);
            }
        }
    }
}
