﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Work
{
    /// <summary>
    /// Interface of RunCommand
    /// </summary>
    public interface IRunCommand
    {
        void RunCommand(Canvas objCanvas);
    }

}
