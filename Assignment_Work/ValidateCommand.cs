﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Work
{
    public class ValidateCommand
    {
        /// <summary>
        /// IsCommandValid - True or False
        /// </summary>
        public bool IsCommandValid { get; set; }

        /// <summary>
        /// Error Message - Set Validation Notification Message
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Constructor to check command Validation 
        /// </summary>
        /// <param name="commandText"></param>
        public ValidateCommand(string commandText)
        {
            this.IsValidCommand(commandText);
        }

        /// <summary>
        /// Constructor to Check Multiline Command Validation
        /// </summary>
        /// <param name="commandLine"></param>
        public ValidateCommand(string[] commandLine)
        {
            this.IsValidMultiLineCommand(commandLine);
        }


        /// <summary>
        /// Check for entered Command is valid or not
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public bool IsValidCommand(string commandText)
        {
            int length = commandText.Split(' ').Length;
            string command = commandText.Split(' ')[0];
            string parameter = string.Empty;

            if (length > 1)
                parameter = commandText.Split(' ')[1].ToString();


            if (command.Equals("moveto") || command.Equals("drawto") || command.Equals("circle") || command.Equals("rect") || command.Equals("tri") || command.Equals("pen") || command.Equals("fill") || command.Equals("clear") || command.Equals("reset"))
            {
                if (command.Equals("clear") || command.Equals("reset") || command.Equals("tri"))
                {
                    if (length == 1)
                    {
                        this.IsCommandValid = true;
                    }
                    else
                    {
                        this.IsCommandValid = false;
                        this.ErrorMessage = "Command does not required any Parameter";
                    }

                }
                else if (command.Equals("fill"))
                {
                    if (parameter.Equals("on") || parameter.Equals("off"))
                    {
                        this.IsCommandValid = true;
                    }
                    else
                    {
                        this.IsCommandValid = false;
                        this.ErrorMessage = "Command Parameter is Invalid";
                    }
                }
                else if (command.Equals("pen"))
                {
                    if (parameter.Equals("green") || parameter.Equals("red") || parameter.Equals("blue") || parameter.Equals("black"))
                    {
                        this.IsCommandValid = true;
                    }
                    else
                    {
                        this.IsCommandValid = false;
                        this.ErrorMessage = "Pen Color is not supported";
                    }
                }
                else if (command.Equals("circle"))
                {
                    if (!string.IsNullOrEmpty(parameter) && IsIntegar(parameter.Split(',')))
                    {
                        this.IsCommandValid = true;
                    }
                    else
                    {
                        this.IsCommandValid = false;
                        this.ErrorMessage = "Command Parameter is Invalid";
                    }
                }
                else if (command.Equals("moveto") || command.Equals("drawto") || command.Equals("rect"))
                {
                    if (!string.IsNullOrEmpty(parameter) && parameter.Split(',').Length == 2 && IsIntegar(parameter.Split(',')))
                    {
                        this.IsCommandValid = true;
                    }
                    else
                    {
                        this.IsCommandValid = false;
                        this.ErrorMessage = "Command Parameter is Invalid";
                    }
                }

            }
            else
            {
                this.IsCommandValid = false;
                this.ErrorMessage = "Invalid Command";
            }

            return this.IsCommandValid;
        }


        /// <summary>
        /// Check for Multiline Command is valid or not
        /// Set Error Message if not valid
        /// </summary>
        /// <param name="commandLine"></param>
        public void IsValidMultiLineCommand(string[] commandLine)
        {
            StringBuilder errorMessages = new StringBuilder();

            for (int i = 0; i < commandLine.Length; i++)
            {
                string commandText = commandLine[i].ToString().Trim().ToLower();

                if (!IsValidCommand(commandText))
                {
                    errorMessages.AppendLine(this.ErrorMessage + " - Line " + (i + 1));
                }
            }

            if (errorMessages.Length > 0)
            {
                this.IsCommandValid = false;
                this.ErrorMessage = errorMessages.ToString();
            }
            else
            {
                this.IsCommandValid = true;
            }
        }

        /// <summary>
        /// Check for Integer Value
        /// Check Parameter is INT or not
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public bool IsIntegar(string[] parameters)
        {
            bool isValid = true;
            if (parameters.Length > 0)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    int number;
                    if (!Int32.TryParse(parameters[i].ToString(), out number))
                    {
                        isValid = false;
                        break;
                    }
                }
            }
            return isValid;
        }
    }
}
