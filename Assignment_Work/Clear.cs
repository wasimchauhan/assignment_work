﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Assignment_Work
{
    /// <summary>
    /// Responsible for Clear Drawing Area
    /// Clear Command Execution
    /// </summary>
    public class Clear : IRunCommand
    {

        /// <summary>
        /// Constructor with Canvas as Parameter
        /// </summary>
        /// <param name="canvas"></param>
        public Clear(Canvas canvas)
        {
            this.RunCommand(canvas);
        }

        /// <summary>
        /// Clear the Graphics Area
        /// </summary>
        /// <param name="canvas"></param>
        public void RunCommand(Canvas canvas)
        {
            canvas.graphics.Clear(Color.LightGray);
        }

    }
}
