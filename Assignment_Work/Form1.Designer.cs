﻿namespace Assignment_Work
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtProgramWindow = new System.Windows.Forms.RichTextBox();
            this.OutputWindow = new System.Windows.Forms.PictureBox();
            this.txtcommandLine = new System.Windows.Forms.TextBox();
            this.btnRunCommand = new System.Windows.Forms.Button();
            this.btnChkSyntax = new System.Windows.Forms.Button();
            this.lblErrorMsg = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.OutputWindow)).BeginInit();
            this.SuspendLayout();
            // 
            // txtProgramWindow
            // 
            this.txtProgramWindow.Location = new System.Drawing.Point(12, 34);
            this.txtProgramWindow.Name = "txtProgramWindow";
            this.txtProgramWindow.Size = new System.Drawing.Size(374, 441);
            this.txtProgramWindow.TabIndex = 0;
            this.txtProgramWindow.Text = "";
            // 
            // OutputWindow
            // 
            this.OutputWindow.BackColor = System.Drawing.Color.LightGray;
            this.OutputWindow.Location = new System.Drawing.Point(437, 34);
            this.OutputWindow.Name = "OutputWindow";
            this.OutputWindow.Size = new System.Drawing.Size(535, 447);
            this.OutputWindow.TabIndex = 1;
            this.OutputWindow.TabStop = false;
            this.OutputWindow.Paint += new System.Windows.Forms.PaintEventHandler(this.OutputWindow_Paint);
            // 
            // txtcommandLine
            // 
            this.txtcommandLine.Location = new System.Drawing.Point(12, 524);
            this.txtcommandLine.Name = "txtcommandLine";
            this.txtcommandLine.Size = new System.Drawing.Size(600, 20);
            this.txtcommandLine.TabIndex = 2;
            this.txtcommandLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.commandLine_KeyDown);
            // 
            // btnRun
            // 
            this.btnRunCommand.Location = new System.Drawing.Point(12, 572);
            this.btnRunCommand.Name = "btnRun";
            this.btnRunCommand.Size = new System.Drawing.Size(75, 23);
            this.btnRunCommand.TabIndex = 3;
            this.btnRunCommand.Text = "Run";
            this.btnRunCommand.UseVisualStyleBackColor = true;
            this.btnRunCommand.Click += new System.EventHandler(this.btnRunCommand_Click);
            // 
            // btnChkSyntax
            // 
            this.btnChkSyntax.Location = new System.Drawing.Point(118, 572);
            this.btnChkSyntax.Name = "btnChkSyntax";
            this.btnChkSyntax.Size = new System.Drawing.Size(75, 23);
            this.btnChkSyntax.TabIndex = 4;
            this.btnChkSyntax.Text = "Syntax";
            this.btnChkSyntax.UseVisualStyleBackColor = true;
            this.btnChkSyntax.Click += new System.EventHandler(this.btnChkSyntax_Click);
            // 
            // lblErrorMsg
            // 
            this.lblErrorMsg.AutoSize = true;
            this.lblErrorMsg.BackColor = System.Drawing.Color.LightGray;
            this.lblErrorMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorMsg.ForeColor = System.Drawing.Color.Red;
            this.lblErrorMsg.Location = new System.Drawing.Point(442, 41);
            this.lblErrorMsg.Name = "lblErrorMsg";
            this.lblErrorMsg.Size = new System.Drawing.Size(24, 16);
            this.lblErrorMsg.TabIndex = 5;
            this.lblErrorMsg.Text = "    ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 711);
            this.Controls.Add(this.lblErrorMsg);
            this.Controls.Add(this.btnChkSyntax);
            this.Controls.Add(this.btnRunCommand);
            this.Controls.Add(this.txtcommandLine);
            this.Controls.Add(this.OutputWindow);
            this.Controls.Add(this.txtProgramWindow);
            this.Name = "Form1";
            this.Text = "Assignment";
            ((System.ComponentModel.ISupportInitialize)(this.OutputWindow)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txtProgramWindow;
        private System.Windows.Forms.PictureBox OutputWindow;
        private System.Windows.Forms.TextBox txtcommandLine;
        private System.Windows.Forms.Button btnRunCommand;
        private System.Windows.Forms.Button btnChkSyntax;
        private System.Windows.Forms.Label lblErrorMsg;
    }
}

