﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_Work
{
    /// <summary>
    /// Responsible For Run - DrawTo Command 
    /// </summary>
    public class DrawTo : IRunCommand
    {
        /// <summary>
        /// Constructor of Class
        /// </summary>
        /// <param name="canvas"></param>
        public DrawTo(Canvas canvas)
        {
            this.RunCommand(canvas);
        }

        /// <summary>
        /// Drawing Line on Canvas
        /// </summary>
        /// <param name="canvas"></param>
        public void RunCommand(Canvas canvas)
        {
            canvas.graphics.DrawLine(canvas.pen, canvas.xPosition, canvas.yPosition, Convert.ToInt32(canvas.Parameters[0]), Convert.ToInt32(canvas.Parameters[1]));
            canvas.xPosition = Convert.ToInt32(canvas.Parameters[0]);
            canvas.yPosition = Convert.ToInt32(canvas.Parameters[1]);
        }
    }
}
