﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assignment_Work;
using System.Drawing;


namespace AssignmentTestProject
{
    /// <summary>
    /// Test Cases Implementation
    ///TestClearCommand - Test Clear Command
    ///TestMoveToCommand - Test MoveTo Command
    ///TestDrawToCommand - Test DrawTo Command
    ///TestValidCommand - Validate - Valid Command
    ///TestInValidCommand - Validate InValid Command
    ///TestMultiLineValidCommand - Validate MultiLine Valid Command
    ///TestMultiLineInValidCommand - Validate MultiLine InValid Command
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// Test Clear Command
        /// </summary>
        [TestMethod]
        public void TestClearCommand()
        {
            Bitmap outputBitMap = new Bitmap(530, 440);
            Graphics graphics = Graphics.FromImage(outputBitMap);
            Canvas canvas = new Canvas(graphics);
            canvas.Command = "clear";
            Commands obj = new Commands(canvas);

        }

        /// <summary>
        /// Test MoveTo Command
        /// </summary>
        [TestMethod]
        public void TestMoveToCommand()
        {
            Bitmap outputBitMap = new Bitmap(530, 440);
            Graphics graphics = Graphics.FromImage(outputBitMap);

            Canvas canvas = new Canvas(graphics);
            canvas.Command = "moveto";
            canvas.Parameters = new string[] { "100", "200" };

            Commands obj = new Commands(canvas);

            Assert.AreEqual(100, canvas.xPosition, 0.01);
            Assert.AreEqual(200, canvas.yPosition, 0.01);

        }

        /// <summary>
        /// Test DrawTo Command
        /// </summary>
        [TestMethod]
        public void TestDrawToCommand()
        {
            Bitmap outputBitMap = new Bitmap(530, 440);
            Graphics graphics = Graphics.FromImage(outputBitMap);

            Canvas canvas = new Canvas(graphics);
            canvas.Command = "drawto";
            canvas.Parameters = new string[] { "50", "50" };

            Commands obj = new Commands(canvas);

            Assert.AreEqual(50, canvas.xPosition, 0.01);
            Assert.AreEqual(50, canvas.yPosition, 0.01);

        }


        /// <summary>
        /// Test Valid Command
        /// </summary>
        [TestMethod]
        public void TestValidCommand()
        {
            ValidateCommand isValidate = new ValidateCommand("circle 100");
            Assert.IsTrue(isValidate.IsCommandValid);
        }


        /// <summary>
        /// Test InValid Command
        /// </summary>
        [TestMethod]
        public void TestInValidCommand()
        {
            ValidateCommand isValidate = new ValidateCommand("rect abc,100");
            Assert.IsFalse(isValidate.IsCommandValid);
            Assert.AreEqual(isValidate.ErrorMessage, "Command Parameter is Invalid");
        }

        /// <summary>
        /// Test Multiline Command  - For Valid Command
        /// </summary>
        [TestMethod]
        public void TestMultiLineValidCommand()
        {
            string[] multilineCommand = new string[] { "moveto 100,100", "drawto 150,150", "circle 100", "rect 80,80" };

            ValidateCommand isValidate = new ValidateCommand(multilineCommand);

            Assert.IsTrue(isValidate.IsCommandValid);
        }

        /// <summary>
        /// Test Multiline Command  - For InValid Command
        /// </summary>
        [TestMethod]
        public void TestMultiLineInValidCommand()
        {
            string[] multilineCommand = new string[] { "moveto 100", "drawto 150,150", "circle 100,abc", "rect 8080" };

            ValidateCommand isValidate = new ValidateCommand(multilineCommand);

            Assert.IsFalse(isValidate.IsCommandValid);

            Assert.AreEqual(isValidate.ErrorMessage, "Command Parameter is Invalid - Line 1\r\nCommand Parameter is Invalid - Line 3\r\nCommand Parameter is Invalid - Line 4\r\n");
        }




    }
}
